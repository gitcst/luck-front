const webpack = require("webpack");
module.exports={
    lintOnSave:false,
    devServer: {
        proxy:{
            '/api': {
                // target: 'http://luck.cstweb.top:820/',
                target: 'http://localhost:820/',
                // pathRewrite:{'^/api':''},
                // ws: true, 默认true
                // changeOrigin: true 默认true
              }
        }
    },
    configureWebpack: {
        //支持jquery
        plugins: [
            new webpack.ProvidePlugin({
                $:"jquery",
                jQuery:"jquery",
                "windows.jQuery":"jquery"
            })
        ]
    },
}


import Vue from 'vue'
import App from './App.vue'
import store from '@/store'
import router from '@/router'
Vue.config.productionTip = false
import Chat from 'vue-beautiful-chat'
import 'element-ui/lib/theme-chalk/index.css';
import './plugins/element.js'
import 'bootstrap';
import 'bootstrap/js/dist/util';
import 'bootstrap/js/dist/alert';
Vue.use(Chat)
import '/public/sb-admin-2.min.css'
Vue.config.productionTip = false

import VueLuckyCanvas from '@lucky-canvas/vue'
Vue.use(VueLuckyCanvas)

Vue.prototype.$_isMobile = function (){
  return navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i);
}

new Vue({
  render: h => h(App),store,router
}).$mount('#app')

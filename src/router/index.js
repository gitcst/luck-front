//配置路由的地方
import Vue from 'vue';
import VueRouter from 'vue-router';

import LuckHome from '@/pages/LuckHome.vue'
import LuckCircle from '@/pages/luck/LuckCircle.vue'
import LuckGrid from '@/pages/luck/LuckGrid.vue'
import Login from '@/components/user/Login.vue'
import Register from '@/components/user/Register.vue'

import UserProfile from '@/pages/user/UserProfile.vue'
import UserInfo from '@/pages/user/UserInfo.vue'
import UserSecurity from '@/pages/user/UserSecurity.vue'
import UserWallet from '@/pages/user/UserWallet.vue'
Vue.use(VueRouter);
let originPush = VueRouter.prototype.push
let originReplace = VueRouter.prototype.replace

VueRouter.prototype.push = function(location,resolve,reject){
    if(resolve && reject){
        originPush.call(this,location,resolve,reject)
    }else{
        originPush.call(this,location,()=>{},()=>{})
    }
}


VueRouter.prototype.replace = function(location,resolve,reject){
    if(resolve && reject){
        originReplace.call(this,location,resolve,reject)
    }else{
        originReplace.call(this,location,()=>{},()=>{})
    }
}
const router = new VueRouter({
    //配置路由
    routes: [
        { path :'/',component : LuckHome, redirect : '/luckCircle', meta: { title:'抽奖平台' }
            ,children: [
                { path: 'user', component: UserProfile, meta: { title:'用户信息' },
                    children : [
                        {path: 'info', component: UserInfo, meta: { title:'用户信息' }},
                        {path: 'security', component: UserSecurity, meta: { title:'账户安全' }},
                        {path: 'wallet', component: UserWallet, meta: { title:'我的钱包' }},
                        ]
                },
                { path: 'luckCircle', component: LuckCircle, meta: { title:'大转盘抽奖' }, },
                { path: 'luckGrid', component: LuckGrid, meta: { title:'九宫格抽奖' }, },
            ]
        },
        { path: "/login",component: Login ,name:'login', meta: { title:'登录' },
        },
        { path: "/register",component: Register ,name:'register', meta: { title:'注册' }},
       
    ]
})

//全局前置路由守卫
router.beforeEach((to,from,next)=>{
    next()
})

//全局后置路由守卫
router.afterEach((to,from)=>{
    document.title = to.meta.title || '抽奖平台'
})

export default router